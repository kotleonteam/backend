'use strict';
module.exports = (sequelize, DataTypes) => {
  const PlaylistItem = sequelize.define('PlaylistItem', {
    playlistId: {
      type: DataTypes.INTEGER,
      field: 'playlist_id'
    },
    name: {
      type: DataTypes.STRING
    },
    url: {
      type: DataTypes.STRING
    },
    duration: {
      type: DataTypes.STRING
    },
    createdAt: {
      type: DataTypes.DATE,
      field: 'created_at'
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: 'updated_at'
    },
    deletedAt: {
      type: DataTypes.DATE,
      field: 'deleted_at'
    }
  }, {
    timestamps: true,
    paranoid: true,
    tableName: 'playlist_items',
    destroyTime: 'deletedAt',
  });

  PlaylistItem.associate = (models) => {
    PlaylistItem.belongsTo(models.Playlist, { foreignKey: 'playlist_id' })
  };

  return PlaylistItem;
};